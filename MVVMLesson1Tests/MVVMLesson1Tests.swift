//
//  MVVMLesson1Tests.swift
//  MVVMLesson1Tests
//
//  Created by Watcharavit Lapinee on 11/28/2560 BE.
//  Copyright © 2560 Watcharavit Lapinee. All rights reserved.
//

import XCTest
@testable import MVVMLesson1

class MVVMLesson1Tests: XCTestCase {
    
    var viewModel: ViewInterface!
    var mockService: MockService!
    
    override func setUp() {
        super.setUp()
        mockService = MockService()
        viewModel = ViewControllerViewModel(service: mockService)
    }


    func test_SaveData_ShouldBeHello() {
        
        
        viewModel.output.showAlert = { message in
            XCTAssertEqual(message, "hello")
            XCTAssertTrue(self.mockService.isCallService)
        }
        
        viewModel.input.saveData(message: "hello")
    }
    
    func test_EmptyString_ShouldError() {
        viewModel.output.didError = { error in
            XCTAssert(true)
        }
        
        viewModel.output.showAlert = { message in
            XCTFail()
        }
        
        viewModel.input.saveData(message: "")
    }
    

}

class MockService: ServiceInterface {
    var didError: ((Error) -> Void)?
    
    
    var isCallService = false
    var isError = false
    
    
    
    func callService(completion: @escaping () -> Void) {
        isCallService = true
        
        if isError {
            didError?(ModelError.emptyString)
        }
        completion()
        
        
    }
}
