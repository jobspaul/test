//
//  TrueService.swift
//  MVVMLesson1
//
//  Created by Watcharavit Lapinee on 11/28/2560 BE.
//  Copyright © 2560 Watcharavit Lapinee. All rights reserved.
//

import UIKit

class TrueService: ServiceInterface {
    var didError: ((Error) -> Void)?

    func callService(completion: @escaping ()-> Void) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            completion()
        }
    }
}
