//
//  File.swift
//  MVVMLesson1
//
//  Created by Watcharavit Lapinee on 11/28/2560 BE.
//  Copyright © 2560 Watcharavit Lapinee. All rights reserved.
//

import Foundation

enum ModelError: Error {
    case emptyString
}

class ViewControllerViewModel: ViewInterface, ViewInteractorOutput {
    
    var input: ViewInteractorInput { return self }
    var output: ViewInteractorOutput { return self }
    
    var showAlert: ((String) -> Void)?
    var hideLoading: (() -> Void)?
    var didError: ((Error) -> Void)?
    
    var service: ServiceInterface!
    
    init(service: ServiceInterface) {
        self.service = service
    }
    
}

extension ViewControllerViewModel: ViewInteractorInput {
    
    func saveData(message: String) {
        service.callService {
        
        if message.isEmpty {
            self.didError?(ModelError.emptyString)

            return
        }
        self.showAlert?(message)
            
        }
        
        service.didError = { error in
            self.didError?(ModelError.emptyString)
        }
    }
    
  
    
}
