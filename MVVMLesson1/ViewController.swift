//
//  ViewController.swift
//  MVVMLesson1
//
//  Created by Watcharavit Lapinee on 11/28/2560 BE.
//  Copyright © 2560 Watcharavit Lapinee. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var txtField: UITextField!
    
    var viewModel: ViewInterface!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let service = TrueService()
        let viewModel = ViewControllerViewModel(service: service)
        configure(viewModel)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func configure(_ interface: ViewInterface) {
        self.viewModel = interface
        
        bindToViewModel()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func submitAction(_ sender: Any) {
        viewModel.input.saveData(message: txtField.text ?? "")
    }
    
}

//MARK: - Binding
extension ViewController {
    
    func bindToViewModel() {
        viewModel.output.showAlert = showAlert()
        viewModel.output.didError = didError()
    }
    
    func didError() -> ((Error) -> Void) {
        return { [weak self] error in
            
        }
    }

    func showAlert() -> ((String) -> Void) {
        return {  [weak self] text in 

            guard let weakSelf = self else { return }

            let alert = UIAlertController(title: "Test", message: text, preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)

            alert.addAction(action)
            weakSelf.present(alert, animated: true, completion: nil)

        }
    }
    
}

