//
//  ViewControllerInteractorIO.swift
//  MVVMLesson1
//
//  Created by Watcharavit Lapinee on 11/28/2560 BE.
//  Copyright © 2560 Watcharavit Lapinee. All rights reserved.
//

import Foundation

protocol ViewInteractorInput {
    func saveData(message: String)
}

protocol ViewInteractorOutput: class {
    
    var showAlert: ((String) -> Void)? { get set }
    var hideLoading: (() -> Void)? { get set }
    var didError: ((Error) -> Void)? { get set }
}

protocol ViewInterface: ViewInteractorInput, ViewInteractorOutput {
    var input: ViewInteractorInput { get }
    var output: ViewInteractorOutput { get }
}

protocol ServiceInterface: class {
    func callService(completion:@escaping () -> Void)
    var didError: ((Error) -> Void)? { get set }
}
